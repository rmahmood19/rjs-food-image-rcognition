This is a demo react app which does Ingredient Recognition on a food image.
Image detection is done by Image Api from CLARIFAI

A working demo of this project is available here;
http://food-recg.surge.sh/
import React, { Component } from 'react';
import Particles from 'react-particles-js';
import Clarifai  from 'clarifai';
import particleOption from './particlesjs-config';
import './App.css';
import Navigation from './components/Navigation/Navigation';
import Logo from './components/Logo/Logo';
import ImgaeLinkForm from './components/ImageLinkForm/ImageLinkForm';
import SignIn from './components/SignIn/SignIn';
import FaceRecognition from './components/FaceRecognition/FaceRecognition';
import Results from './components/Results/Results';

const app = new Clarifai.App({
  apiKey: 'b9f11c881596461abf2cac224d56dcdf'
 });
class App extends Component {
  constructor(){
    super();
    this.state = {
      input :'',
      imageUrl :'',
      results:[],
    }
  };

  displayResults = (response) => {
    console.log(response)
    let val = response.outputs[0].data.concepts
    
    this.setState({results:val})
  }
  onInputChange= (event) =>{
    this.setState({input:event.target.value});
  }
  onSubmit = () => {
    this.setState({imageUrl:this.state.input});
    app.models.predict("aaa03c23b3724a16a56b629203edc62c",
                       this.state.input).then(response => {
                        this.displayResults(response)
                      })
                      .catch(err => console.log(err));
                  }
  render() {
    return (
      <div className="App">
      <Particles 
      params= {particleOption}
      className="particles"/>
      <Logo />
      <Navigation />
      <ImgaeLinkForm 
      onInputChange = {this.onInputChange}
      onSubmit = { this.onSubmit}/>
      <FaceRecognition imageUrl={this.state.input}/>
      <Results results={this.state.results}/>
      </div>
    );
  }
}

export default App;

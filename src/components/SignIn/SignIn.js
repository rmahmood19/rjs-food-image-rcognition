import React from 'react'


const SignIn =()=>{
    return (
    <article class="center mw5 mw6-ns br3 hidden ba mv4 shadow-5">
        <main className="pa4">
            <form className="measure">
                <fieldset id="sign_up" class="ba b--transparent ph0 mh0">
                    <legend className="f4 fw6 ph0 mh0">Sign In</legend>
                    <div className="mt3">
                        <label className="db fw6 lh-copy f6" for="email-address">Email</label>
                        <input className="pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100" type="email" name="email-address" id="email-address"/>
      </div>
                        <div className="mv3">
                            <label className="db fw6 lh-copy f6" for="password">Password</label>
                            <input className="b pa2 input-reset ba bg-transparent hover-bg-black hover-white w-100" type="password" name="password" id="password"/>
      </div>
    </fieldset>
                            <div className="">
                                <input className="b ph3 pv2 input-reset bg-transparent grow pointer f6 dib" type="submit" value="Sign in"/>
    </div>
  </form>
</main>
</article>

    )
};

export default SignIn;
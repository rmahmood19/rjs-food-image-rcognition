import React from 'react';

class FaceRecognition extends React.Component {
    render(){
        return(
            <div className="center ma1">
            <div className="mt3">
            <img alt="" src={this.props.imageUrl} height="300vh"/>
            </div>
            </div>
        );
    }
};
export default FaceRecognition;
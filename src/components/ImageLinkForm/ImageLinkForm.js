import React from 'react';
import "./ImageLinkForm.css"

class IamgeLinkForm extends React.Component {
    render(){
        return (
            <div>
                <p className="f3"> If you provide me a link of food image, I can detect what's in it</p>
                <div className="center ">
                   <div className="form center pa4 br3 shadow-3">
                   <input type="text" className="f4 pa2 w-70 center myinp"
                   onChange={this.props.onInputChange}/>
                   <button className="w-30 grow f4 link ph3 pv2 dib mybtn"
                   onClick={this.props.onSubmit}>Detect</button>
                   </div>
                </div>

            </div>

        );
    }
};
export default IamgeLinkForm;
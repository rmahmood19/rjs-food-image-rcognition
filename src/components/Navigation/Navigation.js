import React from 'react';

class Navigation extends React.Component {
    render(){
        return (
            <nav style={{display:'flex',justifyContent:'flex-end'}}>
                <a className="f4 link dim black pa3 pointer" > Sign Out </a>
            </nav>
        );
    }
}
export default Navigation; 
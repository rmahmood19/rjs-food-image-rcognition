import React from 'react'
import "./logo.css";
import Tilt from 'react-tilt';
import MaskLogo from './robologo2.svg';


class Logo extends React.Component {
    render(){
        return (
            <div className="ma4 mt4 flex justify-center">
                <Tilt className="Tilt grow" options={{ max : 45 }} style={{ height: 80, width: 80 }} >
                <div className="Tilt-inner logo"> <img src={MaskLogo} alt="logo" /> </div>
                </Tilt>
            </div>
        );
    }
}
export default Logo;
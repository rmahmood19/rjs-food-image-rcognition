import React from 'react';

class Results extends React.Component {
    render(){
        return(
            <div className="center ma1">
            <ul>{this.props.results.slice(0, 5).map(item=><li>{item.name}</li>)}</ul>
            </div>
        );
    }
};
export default Results;